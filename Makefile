.PHONY: isort isort-check run install flake check migrate revision parse test

TESTS_DIRS = gd utils

isort:
	isort */*.py

isort-check:
	isort -c */*.py

run:
	uwsgi --http :8000 --wsgi-file app.py --callable api --py-autoreload 1 --attach-daemon "celery -A celery_app worker -B -l info" --attach-daemon "celery -A celery_app beat -l info"

install:
	pip install -r requirements/dev.txt

flake:
	flake8 . --max-line-length=80 --exclude=migrations/versions

check:
	make isort-check && printf "\n\n\n"
	make flake && printf "\n\n\n"

revision:
	alembic revision --autogenerate

migrate:
	alembic upgrade head

parse:
	python -c 'from gd.parse import parse_state; parse_state()'
	python -c 'from tasks.branch_office import parse_branch_office; parse_branch_office()'
	python -c 'from tasks.branch_office import parse_employee_location; parse_employee_location()'
	python -c 'from gd.parse import parse_zip_codes; parse_zip_codes()'

test:
	cd tests && OPBEAT_DISABLE_SEND="1" DATABASE_URL="sqlite:///:memory:" py.test $(TESTS_DIRS) -s
