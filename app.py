import logging.config

import falcon
from falcon_cors import CORS
from opbeat.middleware import Opbeat
from sqlalchemy.exc import SQLAlchemyError

import config
from handlers import sa_session_error_handler
from middleware.authentication import AuthMiddleware
from middleware.json import RequireMediaTypeMiddleware, BodyParserMiddleware
from middleware.opbeat import OpbeatAPMMiddleware
from middleware.session import SessionLifeCycleMiddleware
from resources.application import (
    ApplicationListResource, ApplicationObjectResource,
    ApplicationNoteResource, ApplicationStatusResource, NoteResource,
    ApplicationCollateralResource)
from resources.gd import (
    StateResource, LocationResource, GetLocationByIDResource
)
from resources.user import (
    ADAuthResource, AuthResource, LogoutResource, CustomerProfileResource,
    CustomerRegistrationResource, CustomerCheckEmailResource
)

cors = CORS(
    allow_all_origins=True, allow_all_headers=True,
    allow_all_methods=True, allow_credentials_all_origins=True
)

middleware = [
    OpbeatAPMMiddleware(config.OPBEAT_CLIENT),
    cors.middleware,
    RequireMediaTypeMiddleware(),
    BodyParserMiddleware(),
    AuthMiddleware(),
    # This middleware must be placed the end of list
    SessionLifeCycleMiddleware()
]

api = falcon.API(middleware=middleware)

api.add_route('/ad/auth', ADAuthResource())
api.add_route('/auth', AuthResource())
api.add_route('/logout', LogoutResource())
api.add_route('/applications', ApplicationListResource())
api.add_route('/applications/{idx}', ApplicationObjectResource())
api.add_route('/applications/{idx}/note', ApplicationNoteResource())
api.add_route('/applications/{idx}/collateral', ApplicationCollateralResource())
api.add_route('/notes/{idx}', NoteResource())
api.add_route('/applications/{idx}/status', ApplicationStatusResource())
api.add_route('/customer/profile', CustomerProfileResource())
api.add_route('/customer/registration', CustomerRegistrationResource())
api.add_route('/customer/email/check', CustomerCheckEmailResource())
api.add_route('/geodata/states', StateResource())
api.add_route('/geodata/locations', LocationResource())
api.add_route('/geodata/locations/{idx}', GetLocationByIDResource())

api.add_error_handler(SQLAlchemyError, sa_session_error_handler)
api = Opbeat(api, config.OPBEAT_CLIENT)
logging.config.dictConfig(config.LOGGING)
