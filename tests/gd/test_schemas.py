from db import session
from models import GEOData
from schemas.gd import LocationSchema


def test_location_schema():
    d, errors = LocationSchema().load({'zip': '00000'})
    assert bool(errors)
    assert not d
    d, errors = LocationSchema().load(dict())
    assert bool(errors)
    assert not d
    zip_id = '00001'
    zip_obj = GEOData(zip_id=zip_id, lat=10.1, lad=10.1, state_code='NY')
    session.add(zip_obj)
    session.commit()
    d, errors = LocationSchema().load({'zip': zip_id})
    assert bool(d)
    assert not errors
