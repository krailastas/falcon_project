import csv
import io

from db import session
import requests

from gd.parse import parse_state, STATE_PARSE_URL
from models import GEOState


def test_parse_state():
    assert session.query(GEOState).count() == 0
    parse_state()
    r = requests.get(STATE_PARSE_URL)
    assert r.status_code == 200
    reader = csv.reader(io.StringIO(r.text, newline=''))
    assert session.query(GEOState).count() == sum(1 for _ in reader) - 1
