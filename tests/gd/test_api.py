from db import session
from models import GEOState, BranchOffice, GEOData
from utils.tests import binary_to_dict, http_405


def test_states(client_app):
    path = '/geodata/states'
    headers = {'Content-Type': 'application/json'}
    methods_405 = ['POST', 'PATCH', 'PUT', 'DELETE']

    r = client_app.simulate_get(path, headers=headers)
    assert r._status_code == 200
    content = binary_to_dict(r._content)
    assert content['count'] == session.query(GEOState).count()

    http_405(methods_405, path, client_app, headers)


def test_location_by_id(client_app):
    path = '/geodata/locations/'
    headers = {'Content-Type': 'application/json'}
    methods_405 = ['POST', 'PATCH', 'PUT', 'DELETE']

    r = client_app.simulate_get('{}0'.format(path), headers=headers)
    assert r._status_code == 404

    name_office = 'Test'
    office = BranchOffice(name=name_office, is_active=True)
    session.add(office)
    session.commit()

    path = '{}{}'.format(path, office.id)
    r = client_app.simulate_get(path, headers=headers)
    assert r._status_code == 200
    content = binary_to_dict(r._content)
    assert office.id == content['id']
    assert name_office == content['name']

    http_405(methods_405, path, client_app, headers)


def test_distance(client_app):
    methods_405 = ['POST', 'PATCH', 'PUT', 'DELETE']
    path = '/geodata/locations'
    headers = {'Content-Type': 'application/json'}

    r = client_app.simulate_get(path, headers=headers)
    content = binary_to_dict(r._content)
    assert r._status_code == 400
    assert content['description']['zip'] is not None
    name_office = 'Test'
    office = BranchOffice(name=name_office, is_active=True)
    zip_id = '00502'
    zip_obj = GEOData(zip_id=zip_id, lat=10.1, lad=10.1, state_code='NY')
    session.add(office)
    session.add(zip_obj)
    session.commit()

    r = client_app.simulate_get(
        path, headers=headers, query_string='zip={}'.format(zip_id)
    )
    assert r._status_code == 200

    r = client_app.simulate_get(
        path, headers=headers, query_string='zip=00101'
    )
    assert r._status_code == 400

    http_405(methods_405, path, client_app, headers)
