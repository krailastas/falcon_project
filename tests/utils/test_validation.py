import falcon
import pytest
from marshmallow import (Schema, fields, validate)

from db import session
from models import AnonymousUser, User, UserRole, CustomerProfile
from utils.validation import validate_schema, validate_schema_application


def test_validate_schema():
    class TestSchema(Schema):
        test = fields.Str(required=True, validate=validate.Length(max=5))

    class Req:
        context = {'user': AnonymousUser(), 'data': {'test': None}}
        method = 'POST'

    r = Req()
    with pytest.raises(falcon.HTTPBadRequest):
        validate_schema(TestSchema)(r, None, None, None)

    r.context.update({'data': {'test': 'test'}})
    validate_schema(TestSchema)(r, None, None, None)
    assert r.context['validated_data'] == {'test': 'test'}

    r.context.update({'data': {'test': 'test__'}})
    with pytest.raises(falcon.HTTPBadRequest):
        validate_schema(TestSchema)(r, None, None, None)

    r.method = 'GET'
    with pytest.raises(AttributeError):
        validate_schema(TestSchema)(r, None, None, None)

    r.params = {'test': None}
    with pytest.raises(falcon.HTTPBadRequest):
        validate_schema(TestSchema)(r, None, None, None)

    r.params = {'test': 'test'}
    assert not validate_schema(TestSchema)(r, None, None, None)

    r.params = {'test': 'test__'}
    with pytest.raises(falcon.HTTPBadRequest):
        validate_schema(TestSchema)(r, None, None, None)


def test_validate_schema_application():
    ssn = '123-45-6789'

    class AuthTestSchema(Schema):
        ssn = fields.Str(validate=validate.Regexp(
            regex='^(?P<area>\d{3})[-\ ]?(?P<group>\d{2})[-\ ]?'
                  '(?P<serial>\d{4})$',
            error='Enter a valid U.S. Social Security number in '
                  'XXX-XX-XXXX format.'
        ))

    class UnAuthTestSchema(Schema):
        ssn = fields.Str(validate=validate.Regexp(
            regex='^(?P<area>\d{3})[-\ ]?(?P<group>\d{2})[-\ ]'
                  '?(?P<serial>\d{4})$',
            error='Enter a valid U.S. Social Security number '
                  'in XXX-XX-XXXX format.'
        ), required=True)

    class Req:
        context = {'user': AnonymousUser(), 'data': {'ssn': None}}

    r = Req()
    with pytest.raises(falcon.HTTPBadRequest):
        validate_schema_application(
            AuthTestSchema, UnAuthTestSchema)(r, None, None, None)

    r.context.update({'data': {'ssn': '1234567'}})
    with pytest.raises(falcon.HTTPBadRequest):
        validate_schema_application(
            AuthTestSchema, UnAuthTestSchema)(r, None, None, None)

    r.context.update({'data': {'ssn': ssn}})
    validate_schema_application(
        AuthTestSchema, UnAuthTestSchema)(r, None, None, None)
    assert r.context['validated_data'] == {'ssn': ssn}

    customer = User(email='customer2@test.com', role=UserRole.customer.value)
    employee = User(email='employee2@test.com', role=UserRole.employee.value)
    session.add_all([customer, employee])
    session.commit()

    customer_profile = CustomerProfile(id=customer.id)
    session.add(customer_profile)
    session.commit()

    r.context.update({'data': {'ssn': None}, 'user': customer})
    with pytest.raises(falcon.HTTPBadRequest):
        validate_schema_application(
            AuthTestSchema, UnAuthTestSchema)(r, None, None, None)

    r.context.update({'data': {'ssn': ssn}, 'user': customer})
    validate_schema_application(
        AuthTestSchema, UnAuthTestSchema)(r, None, None, None)
    assert r.context['validated_data'] == {'ssn': ssn}

    session.query(CustomerProfile).filter(
        CustomerProfile.id == customer.id).update({'ssn': ssn})
    session.commit()
    r.context.update({'data': {}, 'user': customer})
    validate_schema_application(
        AuthTestSchema, UnAuthTestSchema)(r, None, None, None)

    r.context.update({'data': {}, 'user': employee})
    with pytest.raises(falcon.HTTPBadRequest):
        validate_schema_application(
            AuthTestSchema, UnAuthTestSchema)(r, None, None, None)

    r.context.update({'data': {'ssn': ssn}, 'user': employee})
    validate_schema_application(
        AuthTestSchema, UnAuthTestSchema)(r, None, None, None)
    assert r.context['validated_data'] == {'ssn': ssn}
