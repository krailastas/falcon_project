import pytest
from db import session
from models import UserRole, User
from utils.db import queryobject_to_dict


def test_db():
    class TestDB:
        q = 1
        w = 2

    with pytest.raises(AttributeError):
        queryobject_to_dict(TestDB)

    user = User(email='user@test.com', role=UserRole.customer.value)
    session.add(user)
    session.commit()

    assert isinstance(queryobject_to_dict(user), dict)

    user_dict = queryobject_to_dict(user, ('id', 'role'))
    assert 'id' not in user_dict.keys()
    assert 'role' not in user_dict.keys()
    assert 'email' in user_dict.keys()
