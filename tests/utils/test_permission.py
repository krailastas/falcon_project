import falcon
import pytest
from db import session
from models import AnonymousUser, User, UserRole
from utils.permission import is_authenticated, is_customer, is_employee


def test_permission():
    class Req:
        context = {'user': AnonymousUser()}

    r = Req()
    with pytest.raises(falcon.HTTPUnauthorized):
        is_authenticated(r, None, None, None)
    with pytest.raises(falcon.HTTPUnauthorized):
        is_customer(r, None, None, None)
    with pytest.raises(falcon.HTTPUnauthorized):
        is_employee(r, None, None, None)

    customer = User(email='customer@test.com', role=UserRole.customer.value)
    employee = User(email='employee@test.com', role=UserRole.employee.value)
    session.add_all([customer, employee])
    session.commit()

    r.context['user'] = customer
    assert is_authenticated(r, None, None, None) is None
    assert is_customer(r, None, None, None) is None
    with pytest.raises(falcon.HTTPUnauthorized):
        is_employee(r, None, None, None)

    r.context['user'] = employee
    assert is_authenticated(r, None, None, None) is None
    assert is_employee(r, None, None, None) is None
    with pytest.raises(falcon.HTTPUnauthorized):
        is_customer(r, None, None, None)
