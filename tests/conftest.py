import pytest
from falcon.testing import TestClient

from db import engine
from models import Base

from ..app import api

metadata = Base.metadata
metadata.bind = engine
metadata.create_all()

test_client_api = TestClient(api.application)


@pytest.fixture(scope='function')
def client_app():
    return test_client_api
