import csv
import io

import requests
from tqdm import tqdm

from db import session
from models import GEOData, GEOState

ZIP_PARSE_URL = 'http://federalgovernmentzipcodes.us/free-zipcode-database.csv'
STATE_PARSE_URL = 'https://raw.githubusercontent.com/ubikuity/List-of-' \
                  'neighboring-states-for-each-US-state/master/usa-states.csv'


def parse_state():
    reader = csv.reader(
        io.StringIO(requests.get(STATE_PARSE_URL).text, newline='')
    )
    list_states = [state_code[0] for state_code in session.query(
        GEOState.state_code).distinct()]
    d = [GEOState(state_code=row[0].strip(), state_name=row[1].strip())
         for row in reader if not row[0].strip() in list_states
         and len(row[0].strip()) == 2]

    session.add_all(d)
    session.commit()


def parse_zip_codes():
    print('Start loading')
    reader = csv.reader(
        io.StringIO(requests.get(ZIP_PARSE_URL).text, newline='')
    )
    print('End loading')
    list_zip_codes = [zip_id[0] for zip_id in session.query(
        GEOData.zip_id).distinct()]

    data_load = []
    zip_parse = []
    for row in tqdm(reader):
        zip_id = row[1].strip()

        if zip_id and zip_id not in list_zip_codes and zip_id not in zip_parse:
            try:
                lat = float(row[6].strip())
                lad = float(row[7].strip())
            except ValueError:
                continue
            obj = GEOData(zip_id=zip_id, lat=lat, lad=lad,
                          state_code=row[4].strip())
            data_load.append(obj)
            if zip_id not in zip_parse:
                zip_parse.append(zip_id)

    session.add_all(data_load)
    session.commit()
