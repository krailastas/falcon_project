from geopy.distance import vincenty

from db import session
from models import BranchOffice, GEOData


def get_branch_offices(zip_id):
    zip_id = zip_id[:5] if zip_id else zip_id
    obj = session.query(GEOData).filter(GEOData.zip_id == zip_id).one_or_none()
    data = []
    if not obj:
        return data
    from_point = (obj.lat, obj.lad)
    is_active = True
    if from_point:
        query = session.query(BranchOffice).filter(
            BranchOffice.is_active == is_active
        )
        for office in query:
            to_point = (office.lat, office.lad)
            data.append(
                {
                    'distance': vincenty(from_point, to_point).miles,
                    'branch_id': office.id,
                    'branch_name': office.name,
                    'city': office.city,
                    'state_code': office.state_code,
                    'state_name': office.state_name,
                    'zip': office.zip,
                    'street': office.street,
                    'lat': office.lat,
                    'lad': office.lad,
                    'phone': office.phone
                }
            )
    return sorted(data, key=lambda k: k['distance'])[:5]
