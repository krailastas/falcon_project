from marshmallow import (Schema, ValidationError, fields, validate, validates,
                         validates_schema)
from sqlalchemy import literal

from db import session
from models import ApplicationOrigination, BranchOffice, GEOData


class BranchSchema(Schema):
    address = fields.Str(validate=validate.Length(max=128))
    city = fields.Str(validate=validate.Length(max=64))
    zip = fields.Str(validate=validate.Regexp(
        regex='^\d{5}(?:-\d{4})?$',
        error='Enter a zip code in the format XXXXX or XXXXX-XXXX.'),
        required=True
    )


class UserBaseSchema(Schema):
    first_name = fields.Str(required=True, validate=validate.Length(max=30))
    last_name = fields.Str(required=True, validate=validate.Length(max=30))
    middle_name = fields.Str(validate=validate.Length(max=30))
    address = fields.Str(validate=validate.Length(max=128))
    city = fields.Str(validate=validate.Length(max=64))
    state = fields.Str(validate=validate.Length(max=2, min=2))
    zip = fields.Str(validate=validate.Regexp(
        regex='^\d{5}(?:-\d{4})?$',
        error='Enter a zip code in the format XXXXX or XXXXX-XXXX.'),
        required=True
    )

    @validates('zip')
    def validate_zip(self, data):
        q = session.query(GEOData).filter(
            GEOData.zip_id == data[:5]
        )
        if not session.query(literal(True)).filter(q.exists()).scalar():
            raise ValidationError('Zip code does not exist', 'zip')

    @validates_schema
    def validate_zip_and_state(self, data):
        zip_id = data.get('zip')
        state = data.get('state')
        if state and zip_id:
            q = session.query(GEOData).filter(
                GEOData.zip_id == zip_id[:5],
                GEOData.state_code == state
            )
            if not session.query(literal(True)).filter(q.exists()).scalar():
                raise ValidationError(
                    'Zip code is invalid for {} state'.format(state), 'zip'
                )


class AuthApplicationSchema(UserBaseSchema):
    phone = fields.Str(
        validate=validate.Regexp(
            regex='^(?:1-?)?(\d{3})[-\.]?(\d{3})[-\.]?(\d{4})$',
            error='Phone number must be in XXX-XXX-XXXX format.'
        ),
        required=True
    )
    phone_type = fields.Int(
        validate=validate.OneOf(
            choices=range(1, 4), error='The value must be between 1 and 3'),
        required=True
    )
    ssn = fields.Str(validate=validate.Regexp(
        regex='^(?P<area>\d{3})[-\ ]?(?P<group>\d{2})[-\ ]?(?P<serial>\d{4})$',
        error='Enter a valid U.S. Social Security number in XXX-XX-XXXX format.'
    ))
    birth_date = fields.Date()
    monthly_net_income = fields.Decimal(required=True, places=2)
    amount_requested = fields.Decimal(required=True, places=2)
    location = fields.Int(required=True)
    current_job = fields.Int(validate=validate.Range(min=1, max=999))
    previous_job = fields.Int(validate=validate.Range(min=1, max=999))
    residence_job = fields.Int(validate=validate.Range(min=1, max=999))
    member = fields.Str(validate=validate.Length(max=128))
    other = fields.Str(validate=validate.Length(max=128))
    btc = fields.Int(
        validate=validate.OneOf(
            choices=range(1, 25), error='The value must be between 1 and 24')
    )
    bdc = fields.Int(
        validate=validate.OneOf(
            choices=range(1, 8), error='The value must be between 1 and 7')
    )
    origination = fields.Int(
        validate=validate.OneOf(
            choices=range(1, 6), error='The value must be between 1 and 5')
    )
    comment = fields.Str(validate=validate.Length(max=500))

    @validates('location')
    def validate_location(self, data):
        q = session.query(BranchOffice).filter(
            BranchOffice.id == data
        )
        if not session.query(literal(True)).filter(q.exists()).scalar():
            raise ValidationError('Branch office does not exist', 'location')

    @validates_schema
    def origination_choice(self, data):
        origination = data.get('origination')
        member = data.get('member')
        other = data.get('other')
        if origination == ApplicationOrigination.MEMBER.value and not member:
            raise ValidationError('Member field is required', 'member')
        elif origination == ApplicationOrigination.OTHER.value and not other:
            raise ValidationError('Other field is required', 'other')


class UnAuthApplicationSchema(AuthApplicationSchema):
    ssn = fields.Str(validate=validate.Regexp(
        regex='^(?P<area>\d{3})[-\ ]?(?P<group>\d{2})[-\ ]?(?P<serial>\d{4})$',
        error='Enter a valid U.S. Social Security number in XXX-XX-XXXX format.'
    ), required=True)


class FilterApplicationSchema(Schema):
    date_start = fields.Date()
    date_end = fields.Date()


class ApplicationNoteSchema(Schema):
    note = fields.Str(validate=validate.Length(max=128), required=True)


class ApplicationStatusSchema(Schema):
    status = fields.Int(
        validate=validate.OneOf(
            choices=range(1, 7), error='The value must be between 1 and 6'),
        required=True
    )
    note = fields.Str(validate=validate.Length(max=128))


class ConsumerGoodsSchema(Schema):
    pieces = fields.Int(required=True)
    category = fields.Str(validate=validate.Length(max=256), required=True)
    description = fields.Str(validate=validate.Length(max=256), required=True)
    value = fields.Decimal(required=True, places=2)


class VehiclesSchema(Schema):
    make = fields.Str(validate=validate.Length(max=128), required=True)
    model = fields.Str(validate=validate.Length(max=128), required=True)
    year = fields.Str(validate=validate.Regexp(
        regex='^(19|20)\d{2}$', error='For 1900-2099'), required=True
    )
    vin = fields.Str(validate=validate.Length(max=128), required=True)
    nada_value = fields.Decimal(required=True, places=2)


class ApplicationCollateralSchema(Schema):
    tp = fields.Int(
        validate=validate.OneOf(
            choices=range(1, 3), error='The value must be between 1 and 2'),
        required=True
    )
    dba = fields.Str(validate=validate.Length(max=128))
    branch = fields.Nested(BranchSchema, required=True)
    consumer_goods = fields.Nested(
        ConsumerGoodsSchema, many=True, allow_none=True, required=True
    )
    vehicles = fields.Nested(
        VehiclesSchema, many=True, allow_none=True, required=True
    )
    borrower = fields.Nested(UserBaseSchema, required=True)
    account_number = fields.Str(
        validate=validate.Length(max=128), required=True
    )
    date_of_loan = fields.Date(required=True)

    @validates_schema
    def max_count(self, data):
        consumer_goods = data.get('consumer_goods')
        vehicles = data.get('vehicles')
        if consumer_goods and len(consumer_goods) > 6:
            raise ValidationError(
                'You exceeded the amount of consumer goods for one application.'
            )
        if vehicles and len(vehicles) > 2:
            raise ValidationError(
                'You exceeded the amount of vehicles for one application.'
            )
