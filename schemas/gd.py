from marshmallow import Schema, ValidationError, fields, validate, validates
from sqlalchemy import literal

from db import session
from models import GEOData


class LocationSchema(Schema):
    zip = fields.Str(validate=validate.Regexp(
        regex='^\d{5}(?:-\d{4})?$',
        error='Enter a zip code in the format XXXXX or XXXXX-XXXX.'),
        required=True
    )

    @validates('zip')
    def validate_zip(self, data):
        q = session.query(GEOData).filter(GEOData.zip_id == data[:5])
        if not session.query(literal(True)).filter(q.exists()).scalar():
            raise ValidationError('Zip code does not exist', 'zip')
