from marshmallow import Schema, fields, validate


class AuthSchema(Schema):
    email = fields.Email(required=True)
    password = fields.Str(required=True, validate=validate.Length(min=6))


class CustomerCheckEmailSchema(Schema):
    email = fields.Email(
        required=True, validate=validate.Length(max=254)
    )


class CustomerRegistrationSchema(Schema):
    email = fields.Email(
        required=True, validate=validate.Length(max=254)
    )
    password = fields.Str(
        required=True, validate=validate.Length(min=6, max=128)
    )
    password2 = fields.Str(
        required=True, validate=validate.Length(min=6, max=128)
    )


class ADAuthSchema(Schema):
    code = fields.Str(required=True, validate=validate.Length(min=1))


class OAUTHTokenSchema(Schema):
    oid = fields.Str(required=True, load_from='oid')
    email = fields.Email(required=True, load_from='userId')
    first_name = fields.Str(required=True, load_from='givenName')
    last_name = fields.Str(required=True, load_from='familyName')
    refresh_token = fields.Str(required=True, load_from='refreshToken')
