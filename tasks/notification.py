from logging import getLogger

from celery_app import celery
from db import session_factory
from models import Notification, NotificationTypes

logger = getLogger('milo-api')


@celery.task
def new_application(idx):
    session = session_factory()
    try:
        data_load = {
            'type_notification': NotificationTypes.CREATED_APPLICATION.value,
            'application_id': idx
        }
        notification = Notification(**data_load)
        session.add(notification)
        session.commit()
    except Exception as exc:
        logger.warning(
            'Retry new_application task of creating '
            'notification for application #{}'.format(idx),
            exc_info=True
        )
        session.rollback()
        new_application.retry(
            args=[idx], exc=exc, countdown=60, max_retries=5
        )
    finally:
        session.close()
