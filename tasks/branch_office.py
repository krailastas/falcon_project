from logging import getLogger

import requests

from celery_app import celery
from db import session_factory
from models import BranchOffice, User, UserRole

BRANCH_OFFICE_URL = 'http://wacapi.carolinawebservices.com/odata/' \
                    'Branches?$expand=BranchOffices/Office/Address/' \
                    'State,BranchPhones/PhoneNumber/PhoneType,' \
                    'BranchEmailAddresses/EmailAddress/EmailAddressType'

EMPLOYEE_LOCATION_URL = 'http://wacapi.carolinawebservices.com/odata/' \
                        'EmployeeLogins?$filter=Login%20eq%20%27' \
                        'AzureAD%5CDavidViolett%27%20and%20Active&$expand=' \
                        'Employee/EmployeeEmailAddresses/EmailAddress/' \
                        'EmailAddressType,EmployeeBranchRoles/Branch,' \
                        'EmployeeBranchRoles/CompanyRole,Employee/' \
                        'EmployeePhones/PhoneNumber/PhoneType'

logger = getLogger('milo-api')


@celery.task
def parse_branch_office():
    session = session_factory()
    try:
        reader = requests.get(BRANCH_OFFICE_URL).json()
        data = reader.get('value')
        list_office = [ids[0] for ids in session.query(
            BranchOffice.id).distinct()]

        data_save = []
        data_update = []
        if data:
            for office in data:
                idx = office['BranchID']
                name = office['BranchName']
                phone = office['BranchPhones'][0][
                    'PhoneNumber']['telephoneNumber']
                phone_type = office['BranchPhones'][0][
                    'PhoneNumber']['PhoneType']['Title']
                lat = office['BranchOffices'][0]['Office']['Address'][
                    'Latitude']
                lad = office['BranchOffices'][0]['Office']['Address'][
                    'Longitude']
                zip = office['BranchOffices'][0]['Office']['Address']['Zip']
                state_code = office['BranchOffices'][0]['Office'][
                    'Address']['State']['StateAbbreviation']
                state_name = office['BranchOffices'][0]['Office'][
                    'Address']['State']['StateName']
                city = office['BranchOffices'][0]['Office']['Address']['City']
                street = office['BranchOffices'][0]['Office'][
                    'Address']['streetLine1']
                is_active = office['BranchOffices'][0]['Office']['Active']

                data_load = {
                    'id': idx, 'name': name, 'lat': lat, 'lad': lad, 'zip': zip,
                    'state_code': state_code, 'state_name': state_name,
                    'city': city, 'street': street, 'is_active': is_active,
                    'phone': phone, 'phone_type': phone_type
                }

                if idx in list_office:
                    data_update.append(data_load)
                else:
                    data_save.append(BranchOffice(**data_load))
            session.add_all(data_save)
            session.bulk_update_mappings(BranchOffice, data_update)
            session.commit()
    except Exception as exc:
        logger.warning('Retry parse_employee_location task', exc_info=True)
        session.rollback()
        parse_branch_office.retry(exc=exc, countdown=300, max_retries=5)
    finally:
        session.close()


@celery.task
def parse_employee_location():
    session = session_factory()
    try:
        reader = requests.get(EMPLOYEE_LOCATION_URL).json()
        data = reader.get('value')

        list_employee = {user.email: user.employee for user in session.query(
            User).filter(User.role == UserRole.employee.value).all()}
        list_office = {office.id: office for office in session.query(
            BranchOffice).filter(BranchOffice.is_active == 1).all()}
        if data:
            data_update = []
            for employee in data:
                email = employee['Employee']['EmployeeEmailAddresses'][0][
                    'EmailAddress']['email']
                locations = employee['EmployeeBranchRoles']
                locts = []
                for location in locations:
                    location_id = location['Branch']['BranchID']
                    loct = list_office.get(location_id)
                    if loct:
                        locts.append(loct)
                employee = list_employee.get(email)
                if employee:
                    employee.locations = locts

                    data_update.append(employee)
            session.add_all(data_update)
            session.commit()
    except Exception as exc:
        logger.warning('Retry parse_employee_location task', exc_info=True)
        session.rollback()
        parse_employee_location.retry(exc=exc, countdown=300, max_retries=5)
    finally:
        session.close()
