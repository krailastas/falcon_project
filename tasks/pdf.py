import io
from logging import getLogger

from azure.storage import CorsRule
from azure.storage.blob import BlockBlobService, ContentSettings

import config
from celery_app import celery
from db import session_factory
from models import Application, ApplicationOrigination, CollateralDocument
from utils.pdf_generator import html_to_pdf

logger = getLogger('milo-api')

block_blob_service = BlockBlobService(
    account_name=config.BLOB_ACCOUNT_NAME,
    account_key=config.BLOB_ACCOUNT_KEY
)
block_blob_service.set_blob_service_properties(
    cors=[CorsRule(
        allowed_origins=config.ALLOWED_ORIGINS,
        allowed_methods=['GET', 'HEAD']
    )]
)


@celery.task
def send_pdf_to_storage(idx):
    session = session_factory()

    try:
        obj = session.query(Application).filter(
            Application.id == idx).one_or_none()
        if obj:
            container_name = config.BLOB_CONTAINER_PDF_NAME
            blob_name = '{}{}.pdf'.format(obj.token, obj.id)
            cxt = {'data': obj, 'app_origination': ApplicationOrigination}
            template = 'credit_application.html'
            stream = io.BytesIO(html_to_pdf(cxt, template))

            content_settings = ContentSettings(content_type='application/pdf')
            block_blob_service.create_blob_from_stream(
                container_name, blob_name, stream,
                content_settings=content_settings
            )
    except Exception as exc:
        session.rollback()
        logger.warning(
            'Retry send_pdf_to_storage task for application #{}'.format(idx),
            exc_info=True
        )
        send_pdf_to_storage.retry(
            args=[idx], exc=exc, countdown=300, max_retries=10
        )
    finally:
        session.close()


@celery.task
def send_collateral_pdf(idx):
    session = session_factory()

    try:
        application = session.query(Application).filter(
            Application.id == idx).one_or_none()
        if application:
            collateral = session.query(CollateralDocument).filter(
                CollateralDocument.id == idx).one_or_none()
            container_name = config.BLOB_COLLATERAL_PDF_NAME
            blob_name = '{}{}.pdf'.format(application.id, application.token)
            cxt = {'collateral': collateral, 'application': application}
            template = 'collateral_document.html'
            stream = io.BytesIO(html_to_pdf(cxt, template))

            content_settings = ContentSettings(content_type='application/pdf')
            block_blob_service.create_blob_from_stream(
                container_name, blob_name, stream,
                content_settings=content_settings
            )
    except Exception as exc:
        session.rollback()
        logger.warning(
            'Retry send_collateral_pdf task for application #{}'.format(idx),
            exc_info=True
        )
        send_collateral_pdf.retry(
            args=[idx], exc=exc, countdown=300, max_retries=10
        )
    finally:
        session.close()
