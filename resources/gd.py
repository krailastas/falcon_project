import falcon
from sqlalchemy import literal

from db import session
from gd.geo_core import get_branch_offices
from models import BranchOffice, GEOState
from schemas.gd import LocationSchema
from utils.db import queryobject_to_dict
from utils.validation import validate_schema


class StateResource:
    def on_get(self, req, resp):
        query = session.query(GEOState)
        resp.data = {
            'count': query.count(),
            'results': [queryobject_to_dict(obj) for obj in query.all()]
        }


class LocationResource:
    @falcon.before(validate_schema(LocationSchema))
    def on_get(self, req, resp):
        data_response = get_branch_offices(req.context['validated_data']['zip'])
        resp.data = {
            'count': len(data_response),
            'results': data_response
        }


class GetLocationByIDResource:
    def on_get(self, req, resp, idx):
        idx = int(idx) if idx.isdigit() else 0
        q = session.query(BranchOffice).filter(BranchOffice.id == idx)
        if not session.query(literal(True)).filter(q.exists()).scalar():
            raise falcon.HTTPNotFound()
        resp.data = queryobject_to_dict(q.one(), ('is_active',))
