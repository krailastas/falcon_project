import falcon
from sqlalchemy import literal

import config
from db import session
from models import (Application, CollateralDocument, ConsumerGood,
                    CustomerProfile, Note, StatusHistory, User, UserRole,
                    Vehicle)
from schemas.application import (ApplicationCollateralSchema,
                                 ApplicationNoteSchema,
                                 ApplicationStatusSchema,
                                 AuthApplicationSchema,
                                 FilterApplicationSchema,
                                 UnAuthApplicationSchema)
from tasks.notification import new_application
from tasks.pdf import send_collateral_pdf, send_pdf_to_storage
from utils.base import to_int
from utils.db import queryobject_to_dict
from utils.permission import is_authenticated, is_employee
from utils.validation import validate_schema, validate_schema_application

FIELDS_EXCLUDE = (
    'ssn', 'user', 'created_at', 'updated_at', 'birth_date', 'token',
    'employee'
)


class ApplicationObjectMixin:
    def application_object(self, user, idx):
        q = session.query(Application).filter(Application.id == idx)
        if user.role == UserRole.customer.value:
            q = q.filter(Application.user == user.id)

        elif user.role == UserRole.employee.value:
            employee = user.employee
            q = q.filter(Application.location.in_(
                [location.id for location in employee.locations]))

        if not session.query(literal(True)).filter(q.exists()).scalar():
            raise falcon.HTTPNotFound()

        return q

    def add_notes(self, query, user):
        results = []
        if user.role == UserRole.customer.value:
            results = [queryobject_to_dict(obj, FIELDS_EXCLUDE)
                       for obj in query]
        elif user.role == UserRole.employee.value:
            for obj in query:
                item = queryobject_to_dict(obj, FIELDS_EXCLUDE)
                notes = [queryobject_to_dict(
                    note, ('application_id', 'created_at'))
                         for note in obj.notes]
                item.update({'notes': notes})
                results.append(item)
        return results


class ApplicationListResource(ApplicationObjectMixin):
    user_fields = ('first_name', 'middle_name', 'last_name')

    def filter_data(self, data, fields):
        return {field: data[field] for field in fields
                if field in data and data.get(field)}

    @falcon.before(validate_schema_application(
        AuthApplicationSchema, UnAuthApplicationSchema
    ))
    def on_post(self, req, resp):
        data = req.context['validated_data']
        user = req.context['user']

        if user.is_authenticated():
            if user.role == UserRole.employee.value:
                customer = session.query(User).filter(
                    User.email == data['email'],
                    User.role == UserRole.customer.value,
                    User.is_active == 1
                ).one_or_none()
                customer = customer.id if customer else None
                data.update(
                    {'employee': user.id, 'user': customer}
                )
            elif user.role == UserRole.customer.value:
                data.update({'user': user.id})
                user_data = self.filter_data(data, self.user_fields)
                session.query(User).filter(User.id == user.id).update(user_data)

                p = session.query(CustomerProfile).filter(
                    CustomerProfile.id == user.id)
                profile = p.one_or_none()
                profile_fields = ['ssn', 'birth_date', 'monthly_net_income',
                                  'phone', 'phone_type', 'address', 'city',
                                  'state', 'zip', 'btc', 'bdc']
                if profile and profile.ssn:
                    profile_fields.remove('ssn')
                profile_data = self.filter_data(data, profile_fields)
                if profile:
                    p.update(profile_data)
                else:
                    profile_data.update({'id': user.id})
                    profile = CustomerProfile(**profile_data)
                    session.add(profile)

        application = Application(**data)
        application.create_token()
        session.add(application)
        session.commit()

        send_pdf_to_storage.apply_async(args=[application.id])
        new_application.apply_async(args=[application.id])

        resp.data = queryobject_to_dict(application, FIELDS_EXCLUDE)
        resp.status = falcon.HTTP_201

    def get_query(self, req):
        params = req.params
        user = req.context['user']
        q = session.query(Application).filter()
        if user.role == UserRole.customer.value:
            q = q.filter(Application.user == user.id)
        elif user.role == UserRole.employee.value:
            employee = user.employee
            q = q.filter(Application.location.in_(
                [location.id for location in employee.locations]))

        d, errors = FilterApplicationSchema().load(params)

        if not errors:
            data_start = d.get('date_start')
            data_end = d.get('date_end')

            if data_start and data_end and data_start > data_end:
                raise falcon.HTTPBadRequest(
                    'Bad Request',
                    'date_start can\'t be greater than date_end'
                )

            if data_start:
                q = q.filter(Application.created_at >= params['date_start'])

            if data_end:
                q = q.filter(Application.created_at <= params['date_end'])

        return q

    @falcon.before(is_authenticated)
    def on_get(self, req, resp):
        query = self.get_query(req)
        user = req.context['user']
        resp.data = {
            'count': query.count(), 'results': self.add_notes(query, user)
        }


class ApplicationObjectResource(ApplicationObjectMixin):
    @falcon.before(is_authenticated)
    def on_get(self, req, resp, idx):
        user = req.context['user']
        resp.data = self.add_notes(
            self.application_object(user, to_int(idx)), user
        )[0]


class ApplicationNoteResource(ApplicationObjectMixin):
    @falcon.before(is_employee)
    @falcon.before(validate_schema(ApplicationNoteSchema))
    def on_post(self, req, resp, idx):
        idx = to_int(idx)
        application = self.application_object(req.context['user'], idx).one()
        if len(application.notes) >= config.MAX_NOTES:
            raise falcon.HTTPBadRequest(
                'Bad request',
                'You exceeded the limit of notes for one application'
            )
        data = req.context['validated_data']
        note = data.get('note')
        note = Note(application_id=idx, text=note)
        session.add(note)
        session.commit()
        resp.data = {
            'id': note.id, 'application_id': idx, 'note': note.text
        }


class NoteResource(ApplicationObjectMixin):
    @falcon.before(is_employee)
    def on_delete(self, req, resp, idx):
        q = session.query(Note).filter(Note.id == to_int(idx))
        if not session.query(literal(True)).filter(q.exists()).scalar():
            raise falcon.HTTPNotFound()

        note = q.one()

        self.application_object(req.context['user'], note.application_id)

        session.delete(note)
        session.commit()
        resp.status = falcon.HTTP_204


class ApplicationStatusResource(ApplicationObjectMixin):
    @falcon.before(is_employee)
    @falcon.before(validate_schema(ApplicationStatusSchema))
    def on_patch(self, req, resp, idx):
        user = req.context['user']
        data = req.context['validated_data']
        status = data['status']
        note = data.get('note')
        idx = to_int(idx)
        application = self.application_object(user, idx).one()
        if application.status != status:
            sh = StatusHistory(
                user_id=user.id, application_id=application.id,
                previous=application.status, current=status
            )
            if note:
                nt = Note(application_id=application.id, text=note)
                session.add(nt)
            application.status = status
            session.add_all([application, sh])
            session.commit()
        resp.data = {'id': idx, 'status': data['status']}


class ApplicationCollateralResource(ApplicationObjectMixin):
    @falcon.before(is_employee)
    def on_get(self, req, resp, idx):
        user = req.context['user']
        idx = to_int(idx)
        application = self.application_object(user, idx).one()
        collateral = application.collateral
        consumer_goods = []
        vehicles = []
        if collateral:
            consumer_goods = [queryobject_to_dict(obj, ('collateral_id', 'id'))
                              for obj in collateral.consumer_goods]
            vehicles = [queryobject_to_dict(obj, ('collateral_id', 'id'))
                        for obj in collateral.vehicles]

        resp.data = {'consumer_goods': consumer_goods, 'vehicles': vehicles}

    @falcon.before(is_employee)
    @falcon.before(validate_schema(ApplicationCollateralSchema))
    def on_post(self, req, resp, idx):
        user = req.context['user']
        data = req.context['validated_data']
        idx = to_int(idx)
        application = self.application_object(user, idx).one()
        data_base = {
            'dba': data.get('dba'), 'tp': data.get('tp'),
            'account_number': data.get('account_number'),
            'date_of_loan': data.get('date_of_loan'),
            'id': idx
        }
        q = session.query(CollateralDocument).filter(
            CollateralDocument.id == idx)
        if session.query(literal(True)).filter(q.exists()).scalar():
            q.update(data_base)
            session.query(ConsumerGood).filter(
                ConsumerGood.collateral_id == idx).delete()
            session.query(Vehicle).filter(Vehicle.collateral_id == idx).delete()
        else:
            collateral = CollateralDocument(**data_base)
            session.add(collateral)

        session.commit()

        consumer_goods = data.get('consumer_goods')
        data_all = []
        if consumer_goods:
            for consumer_good in consumer_goods:
                consumer_good.update({'collateral_id': idx})
                data_all.append(ConsumerGood(**consumer_good))
        vehicles = data.get('vehicles')
        if vehicles:
            for vehicle in vehicles:
                vehicle.update({'collateral_id': idx})
                data_all.append(Vehicle(**vehicle))
        session.add_all(data_all)

        borrower = data['borrower']

        application.first_name = borrower.get('first_name')
        application.last_name = borrower.get('last_name')
        application.middle_name = borrower.get('middle_name')
        application.address = borrower.get('address')
        application.city = borrower.get('city')
        application.state = borrower.get('state')
        application.zip = borrower.get('zip')
        session.add(application)

        customer = application.user
        if customer:
            customer.first_name = borrower.get('first_name')
            customer.last_name = borrower.get('last_name')
            customer.middle_name = borrower.get('middle_name')
            session.add(customer)

        session.commit()

        send_pdf_to_storage.apply_async(args=[application.id])
        send_collateral_pdf.apply_async(args=[application.id])

        resp.status = falcon.HTTP_201
