from datetime import datetime

import falcon
from adal import AuthenticationContext, adal_error
from sqlalchemy import literal

import config
from db import session
from models import (CustomerProfile, EmployeeProfile, User, UserRole,
                    UserSession)
from schemas.user import (ADAuthSchema, AuthSchema, CustomerCheckEmailSchema,
                          CustomerRegistrationSchema, OAUTHTokenSchema)
from utils.db import queryobject_to_dict
from utils.permission import is_authenticated, is_customer
from utils.validation import validate_schema


class AuthResource:
    @falcon.before(validate_schema(AuthSchema))
    def on_post(self, req, resp):
        data = req.context['validated_data']
        user = session.query(User).filter(
            User.email == data['email'],
            User.role == UserRole.customer.value
        ).one_or_none()
        if user and user.check_password(data['password']):
            user_session = UserSession()
            user_session.create_session(user.id)
            session.query(User).filter(User.id == user.id).update(
                {'last_login': datetime.utcnow()}
            )
            session.add(user_session)
            session.commit()
            resp.data = queryobject_to_dict(user_session, ('id', 'created_at'))
        else:
            raise falcon.HTTPBadRequest(
                'Bad Request',
                'Unable to login with provided credentials.'
            )


class CustomerCheckEmailResource:
    @falcon.before(validate_schema(CustomerCheckEmailSchema))
    def on_post(self, req, resp):
        data = req.context['validated_data']
        q = session.query(User).filter(
            User.email == data['email'],
            User.role == UserRole.customer.value
        )
        resp.data = {
            'exists': True if session.query(
                literal(True)).filter(q.exists()).scalar() else False
        }


class CustomerProfileResource:
    @falcon.before(is_customer)
    def on_get(self, req, resp):
        user = req.context['user']
        load_data = queryobject_to_dict(
            user,
            ('id', 'password', 'email', 'is_active', 'role',
             'last_login', 'created_at', 'updated_at')
        )
        profile = session.query(CustomerProfile).filter(
            CustomerProfile.id == user.id
        ).one_or_none()
        if profile:
            load_data.update(
                queryobject_to_dict(profile, ('ssn', 'birth_date', 'id'))
            )
        resp.data = load_data


class CustomerRegistrationResource:
    @falcon.before(validate_schema(CustomerRegistrationSchema))
    def on_post(self, req, resp):
        data = req.context['validated_data']

        if data['password'] != data['password2']:
            raise falcon.HTTPBadRequest(
                'Bad Request',
                'Password does not match the confirm password.'
            )

        q = session.query(User).filter(
            User.email == data['email'],
            User.role == UserRole.customer.value
        )
        if session.query(literal(True)).filter(q.exists()).scalar():
            raise falcon.HTTPBadRequest(
                'Bad Request',
                'Customer with such email already exists'
            )
        user = User(email=data['email'], role=UserRole.customer.value)
        user.set_password(data['password'])
        session.add(user)
        session.commit()
        user_profile = CustomerProfile(id=user.id)
        user_session = UserSession()
        user_session.create_session(user.id)
        session.add(user_profile)
        session.add(user_session)
        session.commit()
        resp.data = queryobject_to_dict(user_session, ('id', 'created_at'))


class ADAuthResource:
    def _acquire_token(self, code):
        auth_context = AuthenticationContext(config.AD_AUTHORITY_URL)

        try:
            data = auth_context.acquire_token_with_authorization_code(
                code, config.AD_REDIRECT_URI, config.AD_RESOURCE,
                config.AD_CLIENT_ID, config.AD_SECRET_ID
            )
            d, errors = OAUTHTokenSchema().load(data)
            if errors:
                raise falcon.HTTPBadRequest(
                    'Bad Request',
                    'Something went wrong with Active Directory'
                )

            return d
        except (adal_error.AdalError, ValueError):
            raise falcon.HTTPBadRequest(
                'Bad Request',
                'Unable to login with provided code.'
            )

    def _get_or_create_employee(self, email, first_name, last_name, oid,
                                refresh_token):
        user = session.query(User).filter(
            User.email == email,
            User.role == UserRole.employee.value
        ).one_or_none()
        if not user:
            user = User()
            user.create_employee(first_name, last_name, email)
            session.add(user)
            session.commit()
            ep = EmployeeProfile(
                oid=oid, refresh_token=refresh_token, id=user.id
            )
            session.add(ep)
        else:
            # Keep the latest refresh_token to increase probability of
            # access.
            session.query(EmployeeProfile).filter(
                EmployeeProfile.id == user.id
            ).update({'refresh_token': refresh_token})

        session.commit()
        return user

    @falcon.before(validate_schema(ADAuthSchema))
    def on_post(self, req, resp):
        data = self._acquire_token(req.context['validated_data']['code'])
        user = self._get_or_create_employee(
            data['email'], data['first_name'], data['last_name'],
            data['oid'], data['refresh_token']
        )

        user_session = UserSession()
        user_session.create_session(user.id)
        session.query(User).filter(User.id == user.id).update(
            {'last_login': datetime.utcnow()}
        )
        session.add(user_session)
        session.commit()
        resp.data = queryobject_to_dict(
            user_session, ('id', 'created_at')
        )


class LogoutResource:
    @falcon.before(is_authenticated)
    def on_delete(self, req, resp):
        session.query(UserSession).filter(
            UserSession.token == req.context['token']
        ).delete()
        resp.status = falcon.HTTP_204
