from db import session


class SessionLifeCycleMiddleware:
    """The middleware must be placed the end of list."""

    def process_response(self, req, resp, resource):
        # In case of error the `rollback ` method is invoked before
        # calling the `remove` method and registered as `error_handler`
        session.remove()
