from db import session
from models import AnonymousUser, UserSession


class AuthMiddleware:
    def process_request(self, req, resp):
        req.context['user'] = AnonymousUser()

        auth_header = req.get_header('Authorization')
        auth = auth_header.split(' ') if auth_header else []
        if len(auth) == 2 and auth[0].lower() == 'token' and auth[1]:
            token = auth[1]
            req.context['token'] = token
            user_session = session.query(UserSession).filter(
                UserSession.token == token
            ).one_or_none()
            if user_session and user_session.user_obj:
                req.context['user'] = user_session.user_obj
