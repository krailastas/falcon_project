import opbeat.instrumentation.control
from opbeat.utils import build_name_with_http_method_prefix

from config import DEBUG

opbeat_is_debug_mode = DEBUG
opbeat.instrumentation.control.instrument()


class OpbeatAPMMiddleware:
    """
    Falcon Framework middleware to track resources performance @ Opbeat
    """

    def __init__(self, client):
        self.client = client

    def process_request(self, req, resp):
        if not opbeat_is_debug_mode:
            self.client.begin_transaction('milo')

    def process_response(self, req, resp, resource):
        name = '{}.{}'.format(
            resource.__class__.__module__,
            resource.__class__.__name__
        )

        rule = build_name_with_http_method_prefix(name, req)

        try:
            status_code = int(resp.status.split(' ')[0])
        except (IndexError, TypeError):
            status_code = 200

        if not opbeat_is_debug_mode:
            self.client.end_transaction(rule, status_code)
