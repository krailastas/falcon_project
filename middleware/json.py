import json

import falcon


class RequireMediaTypeMiddleware:
    def process_request(self, req, resp):
        if not req.client_accepts_json:
            raise falcon.HTTPNotAcceptable(
                'The API only supports responses encoded as JSON.'
            )

        if req.method in ('POST', 'PUT', 'PATCH'):
            ct = req.content_type
            if (ct and 'application/json' not in ct) or not ct:
                raise falcon.HTTPUnsupportedMediaType(
                    'This API only supports requests encoded as JSON.'
                )


class BodyParserMiddleware:
    def process_request(self, req, resp):
        # req.stream corresponds to the WSGI wsgi.input environ variable,
        # and allows you to read bytes from the request body.
        #
        # See also: PEP 3333
        req.context['data'] = {}
        if req.content_length in (None, 0):
            # Nothing to do
            return

        if 'application/json' in req.content_type:
            body = req.stream.read().decode('utf-8')
            try:
                req.context['data'] = json.loads(body)
            except (ValueError, UnicodeDecodeError):
                message = 'Request body is not valid \'application/json\''
                raise falcon.HTTPBadRequest('Bad request', message)

    def process_response(self, req, resp, resource):
        if isinstance(resp.data, dict):
            resp.data = json.dumps(resp.data).encode('utf-8')
