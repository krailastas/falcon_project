import uuid
from enum import Enum

import argon2
from sqlalchemy import (Boolean, Column, Date, DateTime, Float, ForeignKey,
                        Integer, String, UniqueConstraint, literal)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

from db import session
from db.compat import utcnow
from models import Base, association_employee_office


class AnonymousUser:
    id = None

    def __str__(self):
        return 'AnonymousUser'

    def is_anonymous(self):
        return True

    def is_authenticated(self):
        return False

    def create_user(self, first_name, last_name, password, email, role):
        raise NotImplementedError(
            'System doesn\'t provide a DB representation for AnonymousUser.'
        )

    def check_password(self, password):
        raise NotImplementedError(
            'System doesn\'t provide a DB representation for AnonymousUser.'
        )

    def set_password(self, password):
        raise NotImplementedError(
            'System doesn\'t provide a DB representation for AnonymousUser.'
        )


class UserRole(Enum):
    employee = 1
    customer = 2


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    first_name = Column(String(30))
    middle_name = Column(String(30))
    last_name = Column(String(30))
    password = Column(String(128))
    # length=254 to be compliant with RFCs 3696 and 5321
    email = Column(String(254), index=True, nullable=False)

    is_active = Column(Boolean, default=True)
    role = Column(Integer, nullable=False)
    last_login = Column(DateTime(timezone=True))
    created_at = Column(DateTime(timezone=True), server_default=utcnow())
    updated_at = Column(DateTime(timezone=True), onupdate=utcnow())
    employee = relationship('EmployeeProfile', uselist=False)
    sessions = relationship('UserSession', back_populates='user_obj')
    customer = relationship('CustomerProfile', uselist=False)

    __table_args__ = (
        UniqueConstraint('email', 'role'),
    )

    def create_user(self, first_name, middle_name, last_name,
                    password, email, role):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.role = role
        self.middle_name = middle_name
        self.set_password(password)

    def create_employee(self, first_name, last_name, email):
        self.create_user(
            first_name and first_name[:30],
            None,
            last_name and last_name[:30],
            # TODO: password generation must be improved
            str(uuid.uuid4()),
            # An error may raise if AD returns email with length more than 254
            email,
            UserRole.employee.value
        )

    def check_password(self, password):
        ph = argon2.PasswordHasher()
        try:
            return ph.verify(self.password, password)
        except (argon2.exceptions.Argon2Error, AttributeError):
            return False

    def set_password(self, password):
        ph = argon2.PasswordHasher()
        self.password = ph.hash(password)

    def is_anonymous(self):
        """
        Always return False. This is a way of comparing User objects to
        anonymous users.
        """
        return False

    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True


class UserSession(Base):
    __tablename__ = 'user_session'

    id = Column(Integer, primary_key=True)
    token = Column(String(), nullable=False)
    user = Column(Integer, ForeignKey('user.id'), nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=utcnow())
    user_obj = relationship('User', back_populates='sessions')

    def create_session(self, user_id):
        while True:
            # TODO: I'd rather do computing of token on database
            token = str(uuid.uuid4())
            q = session.query(UserSession).filter(
                UserSession.token == token
            )
            if not session.query(literal(True)).filter(q.exists()).scalar():
                break
        self.user = user_id
        self.token = token


class EmployeeProfile(Base):
    __tablename__ = 'employee_profile'

    id = Column(
        Integer, ForeignKey('user.id'), nullable=False, primary_key=True
    )
    # Object identifier(ID) of the user object in Azure AD.
    oid = Column(String(64), nullable=False, unique=True)
    # Long-lived token for refreshing the access_token.
    # Caution: It may expire. In such case We will have to reauth the user
    refresh_token = Column(String(1500))
    locations = relationship(
        'BranchOffice', secondary=association_employee_office,
        backref='employees'
    )


class CustomerProfile(Base):
    __tablename__ = 'customer_profile'

    id = Column(
        Integer, ForeignKey('user.id'), nullable=False, primary_key=True
    )
    ssn = Column(String(11))
    birth_date = Column(Date)
    monthly_net_income = Column(Float)
    phone = Column(String(12))
    phone_type = Column(Integer)
    address = Column(String(128))
    city = Column(String(64))
    state = Column(String(64))
    zip = Column(String(11))
    btc = Column(Integer)
    bdc = Column(Integer)

    @hybrid_property
    def is_ssn(self):
        return self.ssn is not None

    @hybrid_property
    def birth_date_response(self):
        return self.birth_date.strftime('%Y-%m-%d') if self.birth_date else None
