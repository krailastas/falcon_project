from sqlalchemy import Column, ForeignKey, Integer, Table

from models import Base

association_employee_office = Table(
    'association_employee_office', Base.metadata,
    Column('employee_id', Integer, ForeignKey('employee_profile.id')),
    Column('location_id', Integer, ForeignKey('branch_office.id'))
)
