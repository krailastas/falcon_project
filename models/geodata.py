from sqlalchemy import Boolean, Column, Float, Integer, String, Text
from sqlalchemy.orm import relationship

from models import Base


class GEOState(Base):
    __tablename__ = 'geo_state'

    state_code = Column(String(2), primary_key=True, index=True)
    state_name = Column(String(64))


class GEOData(Base):
    __tablename__ = 'geo_data'

    zip_id = Column(String(30), primary_key=True, index=True)
    lat = Column(Float)
    lad = Column(Float)
    state_code = Column(String(2), index=True)


class BranchOffice(Base):
    __tablename__ = 'branch_office'

    id = Column(Integer, primary_key=True)
    name = Column(Text)
    zip = Column(String(11))
    lat = Column(Float)
    lad = Column(Float)
    state_code = Column(String(2), index=True)
    state_name = Column(String(64))
    city = Column(String(128))
    street = Column(Text)
    phone = Column(String(16))
    phone_type = Column(String(20))
    is_active = Column(Boolean, default=False)
    applications = relationship('Application', back_populates='location_obj')
