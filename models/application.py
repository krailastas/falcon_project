import uuid
from enum import Enum

from sqlalchemy import (Column, Date, DateTime, Float, ForeignKey, Integer,
                        String, Text, literal)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

import config
from db import session
from db.compat import utcnow
from models import Base

PDF_STORAGE_URL = 'https://{}.blob.core.windows.net/{}/'.format(
    config.BLOB_ACCOUNT_NAME, config.BLOB_CONTAINER_PDF_NAME
)

PDF_COLLATERAL_URL = 'https://{}.blob.core.windows.net/{}/'.format(
    config.BLOB_ACCOUNT_NAME, config.BLOB_COLLATERAL_PDF_NAME
)


class ApplicationStatus(Enum):
    Applied = 1
    Withdrawn = 2
    Denied = 3
    In_Process = 4
    Approved = 5
    Originated = 6


class ApplicationOrigination(Enum):
    DM = 1
    FACEBOOK = 2
    WEBSITE = 3
    OTHER = 4
    MEMBER = 5


class Application(Base):
    __tablename__ = 'application'

    id = Column(Integer, primary_key=True)
    user = Column(Integer, ForeignKey('user.id'))
    employee = Column(Integer, ForeignKey('user.id'))
    first_name = Column(String(30))
    middle_name = Column(String(30))
    last_name = Column(String(30))
    email = Column(String(254), index=True)
    phone = Column(String(12))
    phone_type = Column(Integer)
    address = Column(String(128))
    city = Column(String(64))
    state = Column(String(64))
    zip = Column(String(11))
    ssn = Column(String(11))
    birth_date = Column(Date)
    monthly_net_income = Column(Float)
    amount_requested = Column(Float)
    btc = Column(Integer)
    bdc = Column(Integer)
    origination = Column(Integer)
    comment = Column(Text)
    location = Column(Integer, ForeignKey('branch_office.id'), nullable=False)
    location_obj = relationship('BranchOffice', back_populates='applications')
    current_job = Column(Integer)
    previous_job = Column(Integer)
    residence_job = Column(Integer)
    member = Column(String(128))
    other = Column(String(128))
    created_at = Column(DateTime(timezone=True), server_default=utcnow())
    updated_at = Column(DateTime(timezone=True), onupdate=utcnow())
    token = Column(String(), nullable=False)
    notes = relationship('Note', back_populates='application')
    collateral = relationship(
        'CollateralDocument', uselist=False, back_populates='application'
    )
    status = Column(Integer, default=ApplicationStatus.Applied.value)

    def create_token(self):
        while True:
            token = str(uuid.uuid4())
            q = session.query(Application).filter(
                Application.token == token
            )
            if not session.query(literal(True)).filter(q.exists()).scalar():
                break
        self.token = token

    @hybrid_property
    def applied_date(self):
        return self.created_at.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    @hybrid_property
    def update_date(self):
        return self.updated_at.strftime('%Y-%m-%dT%H:%M:%S.%fZ') \
            if self.updated_at else None

    @hybrid_property
    def birth_date_response(self):
        return self.birth_date.strftime('%Y-%m-%d') if self.birth_date else None

    @hybrid_property
    def document_pdf_link(self):
        return '{}{}{}.pdf'.format(PDF_STORAGE_URL, self.token, self.id)

    @hybrid_property
    def collateral_pdf_link(self):
        collateral_url = '{}{}{}.pdf'.format(
            PDF_COLLATERAL_URL, self.id, self.token
        )
        return collateral_url if self.collateral else None


class Note(Base):
    __tablename__ = 'note'

    id = Column(Integer, primary_key=True)
    text = Column(Text)
    user_id = Column(Integer, ForeignKey('user.id'))
    application_id = Column(
        Integer, ForeignKey('application.id'), nullable=False
    )
    application = relationship('Application', back_populates='notes')
    created_at = Column(DateTime(timezone=True), server_default=utcnow())

    @hybrid_property
    def create_date(self):
        return self.created_at.strftime('%Y-%m-%dT%H:%M:%S.%fZ') \
            if self.created_at else None


class StatusHistory(Base):
    __tablename__ = 'status_history'

    id = Column(Integer, primary_key=True)
    application_id = Column(
        Integer, ForeignKey('application.id'), nullable=False
    )
    user_id = Column(Integer, ForeignKey('user.id'))
    previous = Column(Integer)
    current = Column(Integer)
    created_at = Column(DateTime(timezone=True), server_default=utcnow())


class CollateralDocument(Base):
    __tablename__ = 'collateral_document'

    id = Column(Integer, ForeignKey('application.id'), primary_key=True)
    dba = Column(String(128))
    tp = Column(Integer)
    account_number = Column(String(128))
    date_of_loan = Column(DateTime)
    application = relationship('Application', back_populates='collateral')
    consumer_goods = relationship('ConsumerGood', back_populates='collateral')
    vehicles = relationship('Vehicle', back_populates='collateral')


class ConsumerGood(Base):
    __tablename__ = 'consumer_good'

    id = Column(Integer, primary_key=True)
    collateral_id = Column(
        Integer, ForeignKey('collateral_document.id'), nullable=False
    )
    collateral = relationship(
        'CollateralDocument', back_populates='consumer_goods'
    )
    pieces = Column(Integer)
    category = Column(String(256))
    description = Column(String(256))
    value = Column(Float)


class Vehicle(Base):
    __tablename__ = 'vehicle'

    id = Column(Integer, primary_key=True)
    collateral_id = Column(
        Integer, ForeignKey('collateral_document.id'), nullable=False
    )
    collateral = relationship(
        'CollateralDocument', back_populates='vehicles'
    )
    make = Column(String(128))
    model = Column(String(128))
    year = Column(String(4))
    vin = Column(String(128))
    nada_value = Column(Float)
