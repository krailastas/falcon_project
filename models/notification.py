from enum import Enum

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer

from db.compat import utcnow
from models import Base


class NotificationTypes(Enum):
    CREATED_APPLICATION = 1


class Notification(Base):
    __tablename__ = 'notification'

    id = Column(Integer, primary_key=True)
    application_id = Column(Integer, ForeignKey('application.id'))
    type_notification = Column(Integer)
    created_at = Column(DateTime(timezone=True), server_default=utcnow())
    updated_at = Column(DateTime(timezone=True), onupdate=utcnow())
    is_read = Column(Boolean, default=False)
