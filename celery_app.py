from celery import Celery

import logging.config
import config


celery = Celery('milo-api', broker=config.CELERY_BROKER_URL)
celery.conf.update(config.CELERY)
logging.config.dictConfig(config.LOGGING)

if __name__ == '__main__':
    celery.start()
