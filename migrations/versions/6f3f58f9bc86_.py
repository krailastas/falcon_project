import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '6f3f58f9bc86'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('branch_office',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.Text(), nullable=True),
                    sa.Column('zip', sa.String(length=11), nullable=True),
                    sa.Column('lat', sa.Float(), nullable=True),
                    sa.Column('lad', sa.Float(), nullable=True),
                    sa.Column('state_code', sa.String(length=2), nullable=True),
                    sa.Column('state_name', sa.String(length=64),
                              nullable=True),
                    sa.Column('city', sa.String(length=128), nullable=True),
                    sa.Column('street', sa.Text(), nullable=True),
                    sa.Column('phone', sa.String(length=16), nullable=True),
                    sa.Column('phone_type', sa.String(length=20),
                              nullable=True),
                    sa.Column('is_active', sa.Boolean(), nullable=True),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_branch_office_state_code'), 'branch_office',
                    ['state_code'], unique=False)
    op.create_table('geo_data',
                    sa.Column('zip_id', sa.String(length=30), nullable=False),
                    sa.Column('lat', sa.Float(), nullable=True),
                    sa.Column('lad', sa.Float(), nullable=True),
                    sa.Column('state_code', sa.String(length=2), nullable=True),
                    sa.PrimaryKeyConstraint('zip_id')
                    )
    op.create_index(op.f('ix_geo_data_state_code'), 'geo_data', ['state_code'],
                    unique=False)
    op.create_index(op.f('ix_geo_data_zip_id'), 'geo_data', ['zip_id'],
                    unique=False)
    op.create_table('geo_state',
                    sa.Column('state_code', sa.String(length=2),
                              nullable=False),
                    sa.Column('state_name', sa.String(length=64),
                              nullable=True),
                    sa.PrimaryKeyConstraint('state_code')
                    )
    op.create_index(op.f('ix_geo_state_state_code'), 'geo_state',
                    ['state_code'], unique=False)
    op.create_table('user',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('first_name', sa.String(length=30),
                              nullable=True),
                    sa.Column('middle_name', sa.String(length=30),
                              nullable=True),
                    sa.Column('last_name', sa.String(length=30), nullable=True),
                    sa.Column('password', sa.String(length=128), nullable=True),
                    sa.Column('email', sa.String(length=254), nullable=False),
                    sa.Column('is_active', sa.Boolean(), nullable=True),
                    sa.Column('role', sa.Integer(), nullable=False),
                    sa.Column('last_login', sa.DateTime(timezone=True),
                              nullable=True),
                    sa.Column('created_at', sa.DateTime(timezone=True),
                              server_default=sa.text('GETUTCDATE()'),
                              nullable=True),
                    sa.Column('updated_at', sa.DateTime(timezone=True),
                              nullable=True),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('email', 'role')
                    )
    op.create_index(op.f('ix_user_email'), 'user', ['email'], unique=False)
    op.create_table('application',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('user', sa.Integer(), nullable=True),
                    sa.Column('employee', sa.Integer(), nullable=True),
                    sa.Column('first_name', sa.String(length=30),
                              nullable=True),
                    sa.Column('middle_name', sa.String(length=30),
                              nullable=True),
                    sa.Column('last_name', sa.String(length=30), nullable=True),
                    sa.Column('email', sa.String(length=254), nullable=True),
                    sa.Column('phone', sa.String(length=12), nullable=True),
                    sa.Column('phone_type', sa.Integer(), nullable=True),
                    sa.Column('address', sa.String(length=128), nullable=True),
                    sa.Column('city', sa.String(length=64), nullable=True),
                    sa.Column('state', sa.String(length=64), nullable=True),
                    sa.Column('zip', sa.String(length=11), nullable=True),
                    sa.Column('ssn', sa.String(length=11), nullable=True),
                    sa.Column('birth_date', sa.Date(), nullable=True),
                    sa.Column('monthly_net_income', sa.Float(), nullable=True),
                    sa.Column('amount_requested', sa.Float(), nullable=True),
                    sa.Column('btc', sa.Integer(), nullable=True),
                    sa.Column('bdc', sa.Integer(), nullable=True),
                    sa.Column('origination', sa.Integer(), nullable=True),
                    sa.Column('comment', sa.Text(), nullable=True),
                    sa.Column('location', sa.Integer(), nullable=False),
                    sa.Column('created_at', sa.DateTime(timezone=True),
                              server_default=sa.text('GETUTCDATE()'),
                              nullable=True),
                    sa.Column('updated_at', sa.DateTime(timezone=True),
                              nullable=True),
                    sa.Column('token', sa.String(), nullable=False),
                    sa.Column('status', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['employee'], ['user.id'], ),
                    sa.ForeignKeyConstraint(['location'],
                                            ['branch_office.id'], ),
                    sa.ForeignKeyConstraint(['user'], ['user.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_application_email'), 'application', ['email'],
                    unique=False)
    op.create_table('customer_profile',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('ssn', sa.String(length=11), nullable=True),
                    sa.Column('birth_date', sa.Date(), nullable=True),
                    sa.Column('monthly_net_income', sa.Float(), nullable=True),
                    sa.Column('phone', sa.String(length=12), nullable=True),
                    sa.Column('phone_type', sa.Integer(), nullable=True),
                    sa.Column('address', sa.String(length=128), nullable=True),
                    sa.Column('city', sa.String(length=64), nullable=True),
                    sa.Column('state', sa.String(length=64), nullable=True),
                    sa.Column('zip', sa.String(length=11), nullable=True),
                    sa.Column('btc', sa.Integer(), nullable=True),
                    sa.Column('bdc', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['id'], ['user.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('employee_profile',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('oid', sa.String(length=64), nullable=False),
                    sa.Column('refresh_token', sa.String(length=1500),
                              nullable=True),
                    sa.ForeignKeyConstraint(['id'], ['user.id'], ),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('oid')
                    )
    op.create_table('user_session',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('token', sa.String(), nullable=False),
                    sa.Column('user', sa.Integer(), nullable=False),
                    sa.Column('created_at', sa.DateTime(timezone=True),
                              server_default=sa.text('GETUTCDATE()'),
                              nullable=True),
                    sa.ForeignKeyConstraint(['user'], ['user.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('association_employee_office',
                    sa.Column('employee_id', sa.Integer(), nullable=True),
                    sa.Column('location_id', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['employee_id'],
                                            ['employee_profile.id'], ),
                    sa.ForeignKeyConstraint(['location_id'],
                                            ['branch_office.id'], )
                    )
    op.create_table('collateral_document',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('dba', sa.String(length=128), nullable=True),
                    sa.Column('tp', sa.Integer(), nullable=True),
                    sa.Column('account_number', sa.String(length=128),
                              nullable=True),
                    sa.Column('date_of_loan', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(['id'], ['application.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('note',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('text', sa.Text(), nullable=True),
                    sa.Column('user_id', sa.Integer(), nullable=True),
                    sa.Column('application_id', sa.Integer(), nullable=False),
                    sa.Column('created_at', sa.DateTime(timezone=True),
                              server_default=sa.text('GETUTCDATE()'),
                              nullable=True),
                    sa.ForeignKeyConstraint(['application_id'],
                                            ['application.id'], ),
                    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('notification',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('application_id', sa.Integer(), nullable=True),
                    sa.Column('type_notification', sa.Integer(), nullable=True),
                    sa.Column('created_at', sa.DateTime(timezone=True),
                              server_default=sa.text('GETUTCDATE()'),
                              nullable=True),
                    sa.Column('updated_at', sa.DateTime(timezone=True),
                              nullable=True),
                    sa.Column('is_read', sa.Boolean(), nullable=True),
                    sa.ForeignKeyConstraint(['application_id'],
                                            ['application.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('status_history',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('application_id', sa.Integer(), nullable=False),
                    sa.Column('user_id', sa.Integer(), nullable=True),
                    sa.Column('previous', sa.Integer(), nullable=True),
                    sa.Column('current', sa.Integer(), nullable=True),
                    sa.Column('created_at', sa.DateTime(timezone=True),
                              server_default=sa.text('GETUTCDATE()'),
                              nullable=True),
                    sa.ForeignKeyConstraint(['application_id'],
                                            ['application.id'], ),
                    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('consumer_good',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('collateral_id', sa.Integer(), nullable=False),
                    sa.Column('pieces', sa.Integer(), nullable=True),
                    sa.Column('category', sa.String(length=256), nullable=True),
                    sa.Column('description', sa.String(length=256),
                              nullable=True),
                    sa.Column('value', sa.Float(), nullable=True),
                    sa.ForeignKeyConstraint(['collateral_id'],
                                            ['collateral_document.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('vehicle',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('collateral_id', sa.Integer(), nullable=False),
                    sa.Column('make', sa.String(length=128), nullable=True),
                    sa.Column('model', sa.String(length=128), nullable=True),
                    sa.Column('year', sa.String(length=4), nullable=True),
                    sa.Column('vin', sa.String(length=128), nullable=True),
                    sa.Column('nada_value', sa.Float(), nullable=True),
                    sa.ForeignKeyConstraint(['collateral_id'],
                                            ['collateral_document.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )


def downgrade():
    op.drop_table('vehicle')
    op.drop_table('consumer_good')
    op.drop_table('status_history')
    op.drop_table('notification')
    op.drop_table('note')
    op.drop_table('collateral_document')
    op.drop_table('association_employee_office')
    op.drop_table('user_session')
    op.drop_table('employee_profile')
    op.drop_table('customer_profile')
    op.drop_index(op.f('ix_application_email'), table_name='application')
    op.drop_table('application')
    op.drop_index(op.f('ix_user_email'), table_name='user')
    op.drop_table('user')
    op.drop_index(op.f('ix_geo_state_state_code'), table_name='geo_state')
    op.drop_table('geo_state')
    op.drop_index(op.f('ix_geo_data_zip_id'), table_name='geo_data')
    op.drop_index(op.f('ix_geo_data_state_code'), table_name='geo_data')
    op.drop_table('geo_data')
    op.drop_index(op.f('ix_branch_office_state_code'),
                  table_name='branch_office')
    op.drop_table('branch_office')
