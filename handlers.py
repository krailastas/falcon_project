import logging

import falcon

from db import session

logger = logging.getLogger('milo-api')


def sa_session_error_handler(ex, req, resp, params):
    session.rollback()
    logger.error(
        'An exception has happened while processing the request', exc_info=1
    )
    raise falcon.HTTPInternalServerError(
        'Internal server error',
        'The request failed due to an internal error.'
    )
