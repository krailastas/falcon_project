import os

from celery.schedules import crontab
from opbeat import Client

DEBUG = 'DEBUG' in os.environ

MAX_NOTES = 10

# Database config
DATABASE = {
    'default': os.environ.get('DATABASE_URL')
}

# Opbeat config
OPBEAT_ORGANIZATION_ID = os.environ.get('OPBEAT_ORGANIZATION_ID')
OPBEAT_APP_ID = os.environ.get('OPBEAT_APP_ID')
OPBEAT_SECRET_TOKEN = os.environ.get('OPBEAT_SECRET_TOKEN')

OPBEAT_CLIENT = Client(
    organization_id=OPBEAT_ORGANIZATION_ID,
    app_id=OPBEAT_APP_ID,
    secret_token=OPBEAT_SECRET_TOKEN,
    DEBUG=DEBUG,
    TIMEOUT=5
)

LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format':
                '%(asctime)s %(name)s %(levelname)s %(funcName)s  '
                '%(filename)s:%(lineno)s %(message)s'
        }
    },
    'handlers': {
        'opbeat': {
            'level': 'INFO',
            'class': 'opbeat.handlers.logging.OpbeatHandler',
            'client': OPBEAT_CLIENT
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'opbeat': {
            'handlers': ['opbeat'],
            'level': 'ERROR',
            'propagate': False,
        },
        'milo-api': {
            'level': 'INFO',
            'handlers': ['opbeat'],
            'propagate': False
        },
        'opbeat.errors': {
            'handlers': ['console']
        },
    }
}

if DEBUG:
    LOGGING['loggers']['sqlalchemy.engine'] = {
        'level': 'INFO',
        'handlers': ['console']
    }

# Azure Active Directory
AD_TENANT = os.environ.get('AD_TENANT', '')
AD_CLIENT_ID = os.environ.get('AD_CLIENT_ID')
AD_SECRET_ID = os.environ.get('AD_SECRET_ID')
AD_REDIRECT_URI = os.environ.get('AD_REDIRECT_URI')
AD_RESOURCE = os.environ.get('AD_RESOURCE')
AD_AUTHORITY_URL = 'https://login.microsoftonline.com/' + AD_TENANT

# BLOB storage service
BLOB_ACCOUNT_NAME = os.environ.get('BLOB_ACCOUNT_NAME')
BLOB_CONTAINER_PDF_NAME = os.environ.get('BLOB_CONTAINER_PDF_NAME')
BLOB_COLLATERAL_PDF_NAME = os.environ.get('BLOB_COLLATERAL_PDF_NAME')
BLOB_ACCOUNT_KEY = os.environ.get('BLOB_ACCOUNT_KEY')

# Celery config
CELERY_BROKER_URL = os.environ.get(
    'CELERY_BROKER_URL', 'redis://redis:6379/0'
)

CELERY = {
    'CELERY_ACCEPT_CONTENT': ['json', 'pickle'],
    'CELERY_TIMEZONE': 'UTC',
    'CELERY_TASK_SERIALIZER': 'json',
    'CELERY_RESULT_SERIALIZER': 'json',
    'CELERY_IMPORTS': (
        'tasks.branch_office', 'tasks.pdf', 'tasks.notification'
    ),
    'CELERYBEAT_SCHEDULE': {
        'update_branch_offices': {
            'task': 'tasks.branch_office.parse_branch_office',
            'schedule': crontab(minute=0, hour=0)
        },
        'update_location_for_employee': {
            'task': 'tasks.branch_office.parse_employee_location',
            'schedule': crontab(minute=0, hour=0)
        },
    }
}

ALLOWED_HOSTS = ['milo.hystk.io']

FORCE_SSL = 'FORCE_SSL' in os.environ
PROTOCOL = 'https://' if FORCE_SSL else 'http://'


def make_origins(hosts):
    return [PROTOCOL + host for host in hosts if host]


ALLOWED_ORIGINS = make_origins(ALLOWED_HOSTS)
