import falcon

from db import session
from models import CustomerProfile

TITLE_BAD_REQUEST = 'Bad request'


def validate_schema(schema):
    def inner(req, resp, resource, params):
        if req.method == 'GET':
            d, errors = schema().load(req.params)
        else:
            d, errors = schema().load(req.context['data'])
        if errors:
            raise falcon.HTTPBadRequest(TITLE_BAD_REQUEST, errors)
        else:
            req.context['validated_data'] = d

    return inner


def validate_schema_application(auth_schema, unauth_schema):
    def inner(req, resp, resource, params):
        user = req.context['user']
        if user.is_authenticated():
            d, errors = auth_schema().load(req.context['data'])
            profile = session.query(CustomerProfile).filter(
                CustomerProfile.id == user.id).one_or_none()
            if not (profile and profile.ssn) and not d.get('ssn'):
                d, errors = unauth_schema().load(req.context['data'])
        else:
            d, errors = unauth_schema().load(req.context['data'])

        if errors:
            raise falcon.HTTPBadRequest(TITLE_BAD_REQUEST, errors)
        else:
            req.context['validated_data'] = d

    return inner
