from sqlalchemy import inspect
from sqlalchemy.ext.hybrid import hybrid_property


def queryobject_to_dict(obj, fields_exclude=None):
    if not obj:
        return {}
    dict_ = {}
    for key in obj.__mapper__.c.keys():
        if not key.startswith('_'):
            dict_[key] = getattr(obj, key)

    for key, prop in inspect(obj.__class__).all_orm_descriptors.items():
        if isinstance(prop, hybrid_property):
            dict_[key] = getattr(obj, key)

    if fields_exclude:
        dict_ = {k: v for k, v in dict_.items() if k not in fields_exclude}

    return dict_
