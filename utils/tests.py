from falcon.testing import json


def binary_to_dict(binary):
    json_acceptable_string = binary.decode('utf-8').replace('\'', '"')
    return json.loads(json_acceptable_string)


def http_405(methods, path, client, headers):
    for method in methods:
        r = client.simulate_request(method=method, path=path, headers=headers)
        assert r._status_code == 405
