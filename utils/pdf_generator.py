import weasyprint
from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('templates'))


def html_to_pdf(cxt, template):
    template = env.get_template(template)
    html = template.render(**cxt)
    return weasyprint.HTML(string=html).write_pdf(target=None)
