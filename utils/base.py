def to_int(num, ret=0):
    try:
        return int(num)
    except ValueError:
        return ret
