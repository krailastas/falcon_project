FROM python:3.5
RUN apt-get update && apt-get install -y \
    python3-dev \
    unixodbc \
    unixodbc-dev \
    libgss3 \
    openssl \
    locales

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen

RUN useradd -u 1000 -m -s /bin/bash webapp
RUN mkdir app
WORKDIR /app/

# Install msodbcsql
RUN curl -O https://milodevstaging.blob.core.windows.net/core/msodbcsql-13.0.0.0.tar.gz \
    && tar xf msodbcsql-13.0.0.0.tar.gz \
    && cd msodbcsql-13.0.0.0 && ./install.sh install --force --accept-license \
    && rm -rf /app/msodbcsql-13.0.0.0 /app/msodbcsql-13.0.0.0.tar.gz

# Ensure that Python outputs everything that's printed inside
# the application rather than buffering it.
ENV PYTHONUNBUFFERED 1

ADD . /app/
RUN chown -R webapp:webapp /app
USER webapp
RUN python -m venv env
RUN if [ -s requirements.txt ]; then env/bin/pip install -r requirements.txt; fi
EXPOSE 80
ENTRYPOINT ["/app/env/bin/uwsgi", "--ini", "/app/uwsgi.ini"]
